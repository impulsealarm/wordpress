<?php
/*
* Template name: After document signing
 *
 * @package flatsome
 */

get_header(); ?>

<?php if( has_excerpt() ) { ?>
<div class="page-header">
	<?php the_excerpt(); ?>
</div>
<?php } ?>

<div  class="page-wrapper">
<div class="row">

	
<div id="content" class="large-12 columns" role="main">

		<?php
				if ( is_user_logged_in() ) {
				    $user_ID = get_current_user_id();
					$check_if_signed = "SELECT user_sign_status FROM {$wpdb->prefix}users WHERE ID=".$user_ID;
					$check_if_signed = $wpdb->prepare($check_if_signed,$user_ID);
					$status_of_signature = $wpdb->get_row($check_if_signed);
					if ($status_of_signature->user_sign_status != "signed") {
						$sql = "UPDATE {$wpdb->prefix}users SET user_sign_status=\"signed\" WHERE ID=".$user_ID;
						$sql = $wpdb->prepare($sql,$user_ID);
						$wpdb->query($sql);
						do_action("woopra_track", "signed_contract");
						echo "Document signed successfully. We're redirecting back to your BRIDGE page...";
					} else {
						echo "You already signed the document. We're redirecting back to your BRIDGE page...";
					}
				} else {
				    echo "You must be logged in! We're redirecting back to your BRIDGE page...";
				}
				
							

		 		while ( have_posts() ) : the_post(); ?>

					<script>
						setTimeout(function() {
							window.location.replace("https://impulsealarm.com/bridge/");
						}, 500)
					</script>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template();
				?>

		<?php endwhile; // end of the loop. ?>

</div><!-- #content -->

</div><!-- .row -->
</div><!-- .page-wrapper -->


<?php get_footer(); ?>