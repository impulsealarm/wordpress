<?php
/*
* Template name: Unsubscribe
 *
 * @package flatsome
 */


get_header(); ?>

<?php if( has_excerpt() ) { ?>
    <div class="page-header">
        <?php the_excerpt(); ?>
    </div>
<?php }
$current_user = wp_get_current_user();
global $current_user;
get_currentuserinfo();
?>
    <script>
        jQuery(document).ready(function($){
            jQuery( "#gform_submit_button_9" ).click(function() {
                $.ajax({
                    type: "POST",
                    url: "https://crm.impulsealarm.com/whook.php?email=<?php echo $current_user->user_email ?>"
                }).success(function(data) {
                    alert("uspjeh");
                });
                woopra.track("unsubscribed_clicked", {
                    name: "<?php echo $current_user->display_name ?>",
                    email: "<?php echo $current_user->user_email ?>"
                });
                console.log("clicked also test");
            });
        });
    </script>

    <div  class="page-wrapper">
        <div class="row">


            <div id="content" class="large-12 columns" role="main">

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php get_template_part( 'content', 'page' ); ?>

                    <?php
                    // If comments are open or we have at least one comment, load up the comment template
                    if ( comments_open() || '0' != get_comments_number() )
                        comments_template();
                    ?>

                <?php endwhile; // end of the loop. ?>

            </div><!-- #content -->

        </div><!-- .row -->
    </div><!-- .page-wrapper -->


<?php get_footer(); ?>