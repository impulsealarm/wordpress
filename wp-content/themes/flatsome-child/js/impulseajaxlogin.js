jQuery(document).ready(function($){
    // Perform AJAX login on form submit
    $('form#loginform-impulse').on('submit', function(e){
        e.preventDefault();
        $('form#login p.statusLogin').show().text("Sending user info, please wait...");
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_login_object.ajaxurl,
            data: {
                'action': 'login', //calls wp_ajax_nopriv_ajaxlogin
                'username': $('form#login #user_login').val(),
                'password': $('form#login #user_pass').val(),
                'security': $('form#login #security').val() },
                success: function(data){
                    $('form#loginform-impulse p.statusLogin').text(data.message);
                    console.log(data);
                }
            }
        });
    });
});