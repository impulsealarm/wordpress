<div id="affwp-affiliate-dashboard-referrals" class="affwp-tab-content">

	<h4><?php _e( 'Referrals', 'affiliate-wp' ); ?></h4>

	<?php
	$per_page  = 30;
	$page      = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
	$pages     = absint( ceil( affwp_count_referrals( affwp_get_affiliate_id() ) / $per_page ) );
	$referrals = affiliate_wp()->referrals->get_referrals(
		array(
			'number'       => $per_page,
			'offset'       => $per_page * ( $page - 1 ),
			'affiliate_id' => affwp_get_affiliate_id(),
			'status'       => array( 'paid', 'unpaid', 'rejected' ),
		)
	);
	?>

	<?php do_action( 'affwp_referrals_dashboard_before_table', affwp_get_affiliate_id() ); ?>


	<table id="affwp-affiliate-dashboard-referrals" class="affwp-table">
		<thead>
			<tr>
				<th class="referral-name"><?php _e( 'Name', 'affiliate-wp' ); ?></th>
				<th class="referral-phone"><?php _e( 'Phone', 'affiliate-wp' ); ?></th>
				<th class="referral-email"><?php _e( 'Email', 'affiliate-wp' ); ?></th>
				<th class="referral-city"><?php _e( 'City', 'affiliate-wp' ); ?></th>
				<th class="referral-package"><?php _e( 'Package', 'affiliate-wp' ); ?></th>
				<th class="referral-description"><?php _e( 'Description', 'affiliate-wp' ); ?></th>
				<?php do_action( 'affwp_referrals_dashboard_th' ); ?>
			</tr>
		</thead>
	
		<tbody>
			<?php if ( $referrals ) : ?>
	
				<?php 
					foreach ( $referrals as $referral ) : 
						$referral = apply_filters( 'referral_fields', $referral);
				?>
					<tr>
						<td class="referral-name"><?php echo $referral->referral_name; ?></td>
						<td class="referral-phone"><?php echo $referral->referral_phone; ?></td>
						<td class="referral-email"><?php echo $referral->referral_email; ?></td>
						<td class="referral-city"><?php echo $referral->referral_city; ?></td>
						<td class="referral-package"><?php echo $referral->referral_package; ?></td>
						<td class="referral-description"><?php echo wp_kses_post( nl2br( $referral->description ) ); ?></td>
						<?php do_action( 'affwp_referrals_dashboard_td', $referral ); ?>
					</tr>
				<?php endforeach; ?>
	
			<?php else : ?>
	
				<tr>
					<td colspan="6"><?php _e( 'You have not made any referrals yet.', 'affiliate-wp' ); ?></td>
				</tr>
	
			<?php endif; ?>
		</tbody>
	</table>
	
	<?php do_action( 'affwp_referrals_dashboard_after_table', affwp_get_affiliate_id() ); ?>

	<?php if ( $pages > 1 ) : ?>

		<p class="affwp-pagination">
			<?php
			echo paginate_links(
				array(
					'current'      => $page,
					'total'        => $pages,
					'add_fragment' => '#affwp-affiliate-dashboard-referrals',
					'add_args'     => array(
						'tab' => 'referrals',
					),
				)
			);
			?>
		</p>

	<?php endif; ?>

</div>
	