<?php
/*
* Template name: Reset password
 *
 * @package flatsome
 */


get_header(); 

    global $wpdb;
    
    $error = '';
    $success = '';
    
    if( isset( $_GET['email'] )) 
    {
        $email = $_GET['email'];
        
        if( empty( $email ) ) {
            $error = 'No email address selected.';
        } else if( ! is_email( $email )) {
            $error = 'Invalid username or e-mail address.';
        } else if( ! email_exists($email) ) {
            $error = 'There is no user registered with that email address.';
        } else {
        
            $random_password = wp_generate_password( 12, false );

            $user = get_user_by( 'email', $email );
            
            $update_user = wp_update_user( array (
                    'ID' => $user->ID, 
                    'user_pass' => $random_password
                )
            );

            if( $update_user ) {
                $to = $email;
                $subject = 'Your new password';
                $sender = get_option('name');
                
                $message = 'Your new password is: '.$random_password;
                
                $headers[] = 'MIME-Version: 1.0' . "\r\n";
                $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                $headers[] = "X-Mailer: PHP \r\n";
                $headers[] = 'From: '.$sender.' < '.$email.'>' . "\r\n";
                
                $mail = wp_mail( $to, $subject, $message, $headers );
                if( $mail )
                    $success = 'Check your email address for you new password.';
                    
            } else {
                $error = 'Oops something went wrong updaing your account.';
            }
 
        }
        
        if( ! empty( $error ) )
            echo '<div class="error_login"><strong>ERROR:</strong> '. $error .'</div>';
        
        if( ! empty( $success ) )
            echo '<div class="updated"> '. $success .'</div>';
    }

?>

<form method="post">
                <?php $user_login = isset( $_GET['email'] ); ?>
                <input type="hidden" name="email" id="email" value="<?php echo $email; ?>" >
                <input type="hidden" name="action" value="reset" />
                <div  class="page-wrapper">
                    <div class="row">
                        <div id="content" class="large-12 columns" role="main">
                            <input type="submit" value="Get New Password" class="button" id="submit" />
                        </div>
                    </div>
                </div>
</form>

<?php if( has_excerpt() ) { ?>
    <div class="page-header">
        <?php the_excerpt(); ?>
    </div>
<?php } ?>


    <div  class="page-wrapper">
        <div class="row">


            <div id="content" class="large-12 columns" role="main">

                <?php while ( have_posts() ) : the_post(); ?>

                    <?php get_template_part( 'content', 'page' ); ?>

                    <?php
                    // If comments are open or we have at least one comment, load up the comment template
                    if ( comments_open() || '0' != get_comments_number() )
                        comments_template();
                    ?>

                <?php endwhile; // end of the loop. ?>

            </div><!-- #content -->

        </div><!-- .row -->
    </div><!-- .page-wrapper -->


<?php get_footer(); ?>