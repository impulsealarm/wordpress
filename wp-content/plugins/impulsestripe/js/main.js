jQuery(document).ready(function(){
	
	var test_publishkey = 'pk_live_oLtqownWPScArvxM2ZOSYnp7';
	
	Stripe.setPublishableKey(test_publishkey);
	
	jQuery.validator.addMethod("checkaccountnumber", function (bankAccountNo) {
		var country = jQuery("input[name='country']").val();
		return Stripe.bankAccount.validateAccountNumber(bankAccountNo, country);
	}, "Your bank account number is not valid.");
	
	jQuery.validator.addMethod("checkroutingnumber", function (routingNo) {
		var country = jQuery("input[name='country']").val();
		return Stripe.bankAccount.validateRoutingNumber(routingNo, country);
	}, "Your routing number is not valid.");
    
	jQuery.validator.addMethod("checkCardNumber", function(cardNumber){
		return Stripe.card.validateCardNumber(cardNumber);
	}, "Invalid card number.");
	
	jQuery.validator.addMethod("checkCardCVC", function(cvc){
		return Stripe.card.validateCVC(cvc);
	}, "Invalid CVC.");
	
	jQuery.validator.addMethod("checkCardExpiration", function(exp){
		return Stripe.card.validateExpiry(exp);
	}, "Invalid card expiry date.");
	
	jQuery("#bank_details").validate({
		rules: {
			bank_acc_no: {
            	required:true,
                checkaccountnumber: true,
    	  	},
	        routing_no: {
            	required:true,
                checkroutingnumber: true,
    	  	},
	        email: {
            	required:true,
            	email:true,
	        },
	        dob: {
            	required:true,
            },
            payment_mode:{
            	required:true,
            },
            tos_acceptance:{
            	required:true,
            },
            card_number:{
            	required:true,
            	checkCardNumber:true,
            },
            card_cvc:{
            	required:true,
            	checkCardCVC: true,
            },
            card_exp:{
            	required:true,
            	checkCardExpiration: true,
            }
    	},
    	messages:{
    		bank_acc_no:{
	            required:"Please fill out this field.",
	        },
	        routing_no:{
	            required:"Please fill out this field.",
	        },
	        email:{
				required : "Please fill out this field.",
				email : "Please enter a valid email address",
			},
	        dob:{
	            required:"Please fill out this field.",
	        },
	        payment_mode:{
            	required:"Please select payment mode.",
            },
            tos_acceptance:{
            	required:"Please agreed to the terms and conditions.",
            }
	    },
	    ignore: ".ignore"
    });
	
	jQuery("#submit_bank_detail_form").click(function(e){
		
		e.preventDefault();

		var form = 	jQuery("#bank_details");
		
		if(form.valid()){
			
			var email = jQuery("#email").val();
			
			var postData = {
				action: 'getRealterDetailFromVtiger',
		        email: email
		    }
			
			jQuery.ajax({
		        type:'POST',
		        url : ajax_object.ajax_url,
		        data: postData,
		        dataType:'json',
		        beforeSend: function(){
					jQuery("body").addClass('loading')
						.loader('show', {
							overlay: true
						}
					);
				},
			   success: function(data){
					
					if(data.success){
						
						var result = data.result;
						
						if(result['realtor_id'] > 0){
							
							form.append(jQuery('<input type="hidden" name="firstname" />').val(result['firstname']));
							form.append(jQuery('<input type="hidden" name="lastname" />').val(result['lastname']));
							form.append(jQuery('<input type="hidden" name="stripe_account_id" />').val(result['stripe_account_id']));
							
							var mode = form.find("input[type='radio']:checked").val();
				            
							var mode_details = [];
							
							mode_details['country'] = jQuery("input[name='country']").val();
							mode_details['currency'] = jQuery("input[name='currency']").val();
							
							if(mode == 'card'){
								mode_details['card_number'] = jQuery("input[name='card_number']").val();
								mode_details['card_cvc'] = jQuery("input[name='card_cvc']").val();
								mode_details['card_exp'] = jQuery("input[name='card_exp']").val();
							} else if(mode == 'bank'){
								mode_details['account_number'] = jQuery("input[name='bank_acc_no']").val();
								mode_details['routing_number'] = jQuery("input[name='routing_no']").val();
							}
							
				            CreateToken(mode,mode_details);
				        }else{
						    jQuery('body').loader('hide');
				            jQuery('.validation_error').text("Your email is not registered. Please contact with Impulse Alarm Dealer.").show();
						}
					}
				}
			});
			return false;
		}
	});
	
	
	// 	START: Changes 31Dec,2015 -------------------------------------------------//
	
	
	if(jQuery("input[name='payment_mode']").is(":checked")){
		
		if(jQuery(".payment_mode:checked").val() == 'bank'){
			jQuery(".card_details").hide();
			jQuery(".bank_details").show();
		} else if(jQuery(".payment_mode:checked").val() == 'card'){
			jQuery(".bank_details").hide();
			jQuery(".card_details").show();
		}
	} else {
		jQuery(".bank_details").hide();
		jQuery(".card_details").hide();
	}
	
	// 	END: Changes 31Dec,2015 -------------------------------------------------//
	
	
	jQuery("input[type=radio][name=payment_mode]").change(function(){
		if(jQuery(this).val() == 'bank'){
			jQuery(".card_details").hide();
			jQuery(".bank_details").show("slow");
			removeIgnoreClass("bank");
			addIgnoreClass("card");
		}else if(jQuery(this).val() == 'card'){
			jQuery(".card_details").show("slow");
			jQuery(".bank_details").hide();
			removeIgnoreClass("card");
			addIgnoreClass("bank");
		}
		
	});
	
	// 	START: Changes 31Dec,2015 -------------------------------------------------//
	
	jQuery("#dob").datepicker({
			showAnim: "slideDown",
			dateFormat :"dd/mm/yy",
			changeMonth: true,
	        changeYear: true,
	        yearRange: "-70:-20", 
	    }
	).attr('readonly','readonly'); 
	
	jQuery("#card_exp").datepicker( {
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy/mm',
        yearRange: "-50:+50",
        onClose: function(dateText, inst) { 
            var month = jQuery("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = jQuery("#ui-datepicker-div .ui-datepicker-year :selected").val();
            jQuery(this).datepicker('setDate', new Date(year, month, 1));
        }
	}).attr('readonly','readonly');

	// 	END: Changes 31Dec,2015 -------------------------------------------------//
	
});

function addIgnoreClass(ele){
	if(ele == 'bank'){
		if(!jQuery("#bank_acc_no").hasClass("ignore"))
			jQuery("#bank_acc_no").addClass("ignore");
		
		if(!jQuery("#routing_no").hasClass("ignore"))
			jQuery("#routing_no").addClass("ignore");
	}else if(ele == 'card'){
		if(!jQuery("#card_number").hasClass("ignore"))
			jQuery("#card_number").addClass("ignore");
		
		if(!jQuery("#card_cvc").hasClass("ignore"))
			jQuery("#card_cvc").addClass("ignore");
		
		if(!jQuery("#card_exp").hasClass("ignore"))
			jQuery("#card_exp").addClass("ignore");
	}
}

function removeIgnoreClass(ele){
	if(ele == 'bank'){
		jQuery("#bank_acc_no").removeClass("ignore");
		jQuery("#routing_no").removeClass("ignore");
	} else if(ele == 'card'){
		jQuery("#card_number").removeClass("ignore");
		jQuery("#card_cvc").removeClass("ignore");
		jQuery("#card_exp").removeClass("ignore");
	}
}

function CreateToken(mode, mode_details){
	if(mode == 'bank'){
		Stripe.bankAccount.createToken({
			country: mode_details.country,
			currency: mode_details.currency,
			routing_number: mode_details.routing_number,
			account_number: mode_details.account_number
		}, stripeResponseHandler);
    } else if(mode == 'card') {
    	Stripe.card.createToken({
    		currency: 'usd',
    		number: mode_details.card_number,
    		cvc: mode_details.card_cvc,
    		exp: mode_details.card_exp
    	}, stripeResponseHandler);
    }
}

function stripeResponseHandler(status, response){
	
	var form = jQuery("#bank_details");
	
	if (response.error) {
		jQuery('body').loader('hide');
        form.find('.validation_error').text(response.error.message).show();
        return false;
    } else {
    	if(response.type == "bank_account"){
    		var token = response.id;
		    form.append(jQuery('<input type="hidden" name="stripeToken" />').val(token));
		    form.submit();
		    return true;
    	} else if(response.type == "card"){
    	
    		if(response.card.funding == 'debit'){
				var token = response.id;
			    form.append(jQuery('<input type="hidden" name="stripeToken" />').val(token));
			    form.submit();
			    return true;
	    	} else {
	    		jQuery('body').loader('hide');
	            form.find('.validation_error').text("For transfer we only accept debit card.").show();
	            return false;
	        }
    	}
	}
}