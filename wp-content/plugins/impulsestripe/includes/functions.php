<?php

	function postHttpRequest($ws_url, $postParams = array()){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $ws_url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postParams);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	function getHttpRequest($ws_url, $getParams = array()){
		$url = $ws_url;
		if(!empty($getParams)){
				$url .= '?';
				$i = 0;
				$len = count($getParams) - 1;
				foreach($getParams as $key => $val){
						$url .=  $key;
						$url .= '=';
						$url .= urlencode($val);
						if($i != $len){ 
								$url .= '&';
						}
						$i++;
				}
				unset($i, $len);
		}
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPGET, true);
		
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		$response = curl_exec($ch);
		curl_close($ch);
		return $response;
	}

	function logoutFromInstance($ws_url, $sessionName){

		$getParams = array(
				'operation'=>'logout',
				'sessionName'=>$sessionName
		);
		$response = $this->getHttpRequest($ws_url, $getParams);
		$response = json_decode($response);
		return $response;
	}

	function loginToInstance($ws_url, $username, $accessKey){
		$postParams = array(
				'operation'=>'login',
				'username'=>$username,
				'accessKey'=>$accessKey
		);
		$response = postHttpRequest($ws_url, $postParams);
		$response = json_decode($response);
		return $response;
	}

	function getChallangeObj($ws_url, $username){
		$getParams = array(
				'operation' => 'getchallenge',
				'username' => $username
		);
		$response = getHttpRequest($ws_url, $getParams);
		$response = json_decode($response);
		return $response;
	}

	function login($ws_url , $username, $accessKey){

		$challangeObj = getChallangeObj($ws_url, $username);
		$challangeToken = '';
		if($challangeObj->success){
				$challangeToken = $challangeObj->result->token;
		}
		if($challangeToken != ''){
				$accessKey = md5($challangeToken . $accessKey);

				$sessionObj = loginToInstance($ws_url,$username, $accessKey);
				if($sessionObj->success){
					$_SESSION['uname'] = $sessionObj->username;
					$_SESSION['accesskey'] = $sesionObj->accessKey;
					return $sessionObj->result;
				}
		}
		return false;
	}

?>