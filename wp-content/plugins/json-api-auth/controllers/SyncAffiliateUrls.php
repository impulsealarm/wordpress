<?php

class Affiliate_Sync_Controller {

	public function GetBonusURLShortenLink($affiliate_id){
			
		$urls = array(
			"bonus_url_1" => "https://impulsealarm.com/n5v59anolqv/", 
			"bonus_url_2" => "https://impulsealarm.com/g3cwifp/", 
			"bonus_url_3" => "https://impulsealarm.com/y9kjyycipzw/"
		);
		
		$short_links = array();
		
		foreach($urls as $name => $url){
			
			$args = array('base_url' => $url, "affiliate_id" => $affiliate_id);
			
			$affiliate_referral_url = esc_url( urldecode(affwp_get_affiliate_referral_url($args)));
			
			$short_url_response = $this->createShortenerLink($affiliate_referral_url);
				
			$short_links[$name] = $short_url_response['data']['url'];
		}
		
		return $short_links;
	}
		
	public function createShortenerLink($url){
	
		$url = "https://api-ssl.bitly.com/v3/shorten?access_token=152018dda9d93eaf27c31eca8329311093df660e&longUrl=$url";
		
		$ch = curl_init();
	
		curl_setopt($ch, CURLOPT_URL, $url);
				
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
			
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$response = curl_exec($ch);
		
		curl_close($ch);
		
		return json_decode($response, true);
	}
	
	/**
	 * public function save the affiliate_urls in wordpress of given affiliate_id
	 * @param Int $affiliate_id
	 * @param array $affiliate_urls
	 * @return bool true or false
	 */

	public function saveAffiliateBonusURL($affiliate_id, $affiliate_urls){

		global $wpdb;
		
		$aff_res = $wpdb->get_row("select * from wp_vtiger_affiliate where affiliate_id = $affiliate_id");
		
		$wpdb->query("delete from wp_vtiger_affiliate where affiliate_id = $affiliate_id");
		
		$insert_data = array();
		
		$insert_data['affiliate_id'] = $affiliate_id;
		
		$insert_data['affiliate_data'] = json_encode($affiliate_urls);
		
		$wpdb->insert("wp_vtiger_affiliate", $insert_data);
		
		$response = $wpdb->insert_id;
		
		return $response;
	}					
}