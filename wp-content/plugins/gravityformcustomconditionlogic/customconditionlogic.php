<?php
/**
 * Plugin Name: Gravity Forms Custom Logic
 * Plugin URI: http://devitechnosolutions.com
 * Description: Plugin allows user to Set values of another field based on value of current Field
 * Author: Manish Goyal
 * Author URI: www.devitechnosolutions.com
*/

add_action( 'gform_loaded', array( 'GF_Custom_Logic', 'load' ), 5 );

define( 'CUSTOM_LOGIC__PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'CUSTOM_LOGIC__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

class GF_Custom_Logic {

	public static function load(){

		if ( ! method_exists( 'GFForms', 'include_feed_addon_framework' ) ) {
			return;
		}

		require_once( 'class-gf-custom-condition-logic.php' );

		GFAddOn::register( 'GFCustomLogic' );
		
	}
}

